#include <ctime>
#include <string>

#include <vtkCellArray.h>
#include <vtkMergePoints.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkTimerLog.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkUnstructuredGridWriter.h>

int main(int argc, char** argv)
{
  if(argc < 2)
  {
    std::cout << "Incorrect number of arguemnts" << std::endl;
    std::cout << "Usage : pointmerge <dataset> " << std::endl;
    exit(EXIT_FAILURE);
  }
  std::string dataset(argv[1]);

  // Run the marching cubes filter here, over provided dataset
  vtkSmartPointer<vtkUnstructuredGridReader> inputReader
    = vtkSmartPointer<vtkUnstructuredGridReader>::New();
  inputReader->SetFileName(dataset.c_str());
  inputReader->Update();

  vtkSmartPointer<vtkTimerLog> timer = vtkSmartPointer<vtkTimerLog>::New();
  timer->StartTimer();

  vtkSmartPointer<vtkUnstructuredGrid> intermDataset = inputReader->GetOutput();
  vtkSmartPointer<vtkPoints> intermPoints = intermDataset->GetPoints();
  vtkSmartPointer<vtkCellArray> intermCells = intermDataset->GetCells();

  // Get basic data needed for performing the merge.
  double bounds[6];
  intermDataset->GetBounds(bounds);
  int numCells, numPoints;
  numCells = intermDataset->GetNumberOfCells();
  if(numCells == 0)
  {
    std::cout << "Number of cells is 0, something went wrong" << std::endl;
    exit(EXIT_FAILURE);
  }
  numPoints = intermPoints->GetNumberOfPoints();

  std::cout << "Number of cells to process " << numCells << std::endl;
  std::cout << "Number of points to process " << numPoints << std::endl;

  vtkSmartPointer<vtkPoints> mergedPoints = vtkSmartPointer<vtkPoints>::New();
  //Init locator for merging points.
  vtkSmartPointer<vtkMergePoints> mergeLocator
    = vtkSmartPointer<vtkMergePoints>::New();
  mergeLocator->Initialize();
  mergeLocator->InitPointInsertion(mergedPoints, bounds);

  vtkIdType* mapping = new vtkIdType[numPoints];
  vtkIdType idx;
  //Make mapping of points using the locator.
  for(idx = 0; idx < numPoints; ++idx)
  {
    double point[3];
    vtkIdType index;
    intermPoints->GetPoint(idx, point);
    mergeLocator->InsertUniquePoint(point, index);
    mapping[idx] = index;
  }

  timer->StopTimer();
  std::cout << "Time to finish merging points and generate mapping : "
            << timer->GetElapsedTime() << std::endl;
  timer->StartTimer();

  std::cout << "After processing, number of points "
            << mergeLocator->GetPoints()->GetNumberOfPoints() << std::endl;

  // Create a new dataset for storing new output
  vtkSmartPointer<vtkUnstructuredGrid> newDataSet
    = vtkSmartPointer<vtkUnstructuredGrid>::New();
  newDataSet->Allocate(numCells);
  // Add points to the new dataset
  newDataSet->SetPoints(mergeLocator->GetPoints());
  // Add cell set to the new dataset
  vtkSmartPointer<vtkCellArray> newCellSet = vtkSmartPointer<vtkCellArray>::New();

  // Iterate over cells and then make new cells using new points.
  // Check to see for each cell's connection if the point exists in the locator
  vtkIdType point_cnt;
  vtkIdType *points;
  for (idx = 0, intermCells->InitTraversal() ;
       intermCells->GetNextCell(point_cnt, points) ; ++idx)
  {
    assert(point_cnt == 3);
    for(int i = 0; i < point_cnt; i++)
    { points[i] = mapping[points[i]]; }
    newDataSet->InsertNextCell(intermDataset->GetCellType(idx), point_cnt, points);
  }

  timer->StopTimer();
  std::cout << "Time to build dataset with merged points : "
            << timer->GetElapsedTime() << std::endl;

  /*vtkSmartPointer<vtkUnstructuredGridWriter> outputWriter
    = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
  outputWriter->SetFileName("output.vtk");
  outputWriter->SetInputData(newDataSet);
  outputWriter->Write();*/
}
