#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#ifdef _BUILDING_TBB_
#include <tbb/task_scheduler_init.h>
#endif

#include <vtkm/Bounds.h>
#include <vtkm/Types.h>
#include <vtkm/Range.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/PointLocatorSelfJoin.h>
#include <vtkm/cont/Timer.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/DispatcherMapField.h>

#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/WorkletReduceByKey.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>

#include "PointMergeWorklets.h"
#include "Utilities.hxx"

using Point = vtkm::Vec<vtkm::FloatDefault, 3>;
using IdHandle = vtkm::cont::ArrayHandle<vtkm::Id>;

template <typename DeviceAdapter>
void PointMergeNearestNeighbor(vtkm::cont::ArrayHandle<Point>& points,
                               vtkm::FloatDefault delta,
                               vtkm::Bounds& bounds,
                               IdHandle& globalMap,
                               DeviceAdapter)
{
  using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

  assert(points.GetNumberOfValues() != 0);

  vtkm::Id3 dims =  {64, 64, 64};
  vtkm::cont::PointLocatorSelfJoin locator(
    // Dims over here may improve performance,
    // but I am leaving it at this.
    // There is a tradeoff, if we have more cells, updating search
    // structures become slower than usual.
    bounds, dims, delta);

  IdHandle neighbors;
  vtkm::cont::ArrayHandle<vtkm::FloatDefault> distances;
  // Local map for points that merge together.
  IdHandle localMap;
  {
    // Populate the local and global map to initial values.
    vtkm::cont::ArrayHandleIndex indices(points.GetNumberOfValues());
    DeviceAlgorithm::Copy(indices, localMap);
    DeviceAlgorithm::Copy(indices, globalMap);
  }

  vtkm::cont::Timer<> timer;

  bool converged = false;
  while(!converged)
  {
    // Update PointLocator for merging points for the current round
    vtkm::cont::CoordinateSystem coords("merge", points);
    locator.SetCoords(coords);
    locator.Update();

    std::cout << "Time to update search strcuture : " << timer.GetElapsedTime()
              << std::endl;
    timer.Reset();

    // Get the neighbor for each of the points.
    pointmerge::GetNearestNeighbor nnWorklet(delta);
    using NearestNeighborDispatcher =
       vtkm::worklet::DispatcherMapField<pointmerge::GetNearestNeighbor, DeviceAdapter>;
    NearestNeighborDispatcher nnDispatch(nnWorklet);
    nnDispatch.Invoke(points, locator, neighbors, distances);

    std::cout << "Time to find nearest neighbors : " << timer.GetElapsedTime()
              << std::endl;
    timer.Reset();

    // Check if indices and neighbors are the same.
    // If they are, the merge has converged.
    IdHandle comparison;
    comparison.Allocate(1);
    {
      auto comparePortal = comparison.GetPortalControl();
      comparePortal.Set(0, 0);
    }
    using ComparisonDispatcher =
       vtkm::worklet::DispatcherMapField<pointmerge::CompareWorklet, DeviceAdapter>;
    ComparisonDispatcher().Invoke(neighbors, comparison);

    std::cout << "Time to make decision for next iteration : " << timer.GetElapsedTime()
              << std::endl;
    timer.Reset();
    {
      auto comparePortal = comparison.GetPortalConstControl();
      if(comparePortal.Get(0) == 0)
      {
        converged = true;
        break;
      }
    }
    // Auxiliary arrays to complete reduce by key properly.
    IdHandle updatedMap;
    vtkm::cont::ArrayHandle<Point> mergedPoints;
    vtkm::cont::ArrayHandleIndex pointIndices(points.GetNumberOfValues());
    // The call to reduce by key does 3 things
    // 1. Reduces the points that are neighbors.
    // 2. Updates the global map with the merged neighbors.
    // 3. Reduces the local map for the next round.
    vtkm::worklet::DispatcherReduceByKey<pointmerge::PerformMergeForRound, DeviceAdapter> dispatcher;
    vtkm::worklet::Keys<vtkm::Id> keys(neighbors, DeviceAdapter());
    dispatcher.Invoke(keys, pointIndices, points, localMap, globalMap, mergedPoints, updatedMap);

    vtkm::cont::ArrayHandleIndex indices(globalMap.GetNumberOfValues());
    vtkm::worklet::DispatcherMapField<pointmerge::FixNeighborhoodWorklet, DeviceAdapter> fixDispatcher;
    fixDispatcher.Invoke(indices, globalMap);

    std::cout << "Time to merge points and update maps : " << timer.GetElapsedTime()
              << std::endl;
    timer.Reset();

    // Update points and local map for the next round.
    DeviceAlgorithm::Copy(mergedPoints, points);
    DeviceAlgorithm::Copy(updatedMap, localMap);

    std::cout << "Time to ready data for next iteration : " << timer.GetElapsedTime()
              << std::endl;
    timer.Reset();
  }
  timer.Reset();
  CondenseGlobalMapIndices(globalMap, DeviceAdapter());
  std::cout << "Time to update global map : " << timer.GetElapsedTime()
            << std::endl;
  timer.Reset();
}

int main(int argc, char** argv)
{
  vtkm::cont::ArrayHandle<Point> points;
  IdHandle globalMap;
  vtkm::FloatDefault delta;
  vtkm::Bounds bounds;
  bool real = false;
  std::string datafile;
  vtkm::cont::DataSet dataset;

  ParseAndPopulate(argc, argv, points, delta, bounds, real, datafile);

#ifdef _BUILDING_TBB_
  int numThreads = tbb::task_scheduler_init::default_num_threads();
  std::cout << "Default TBB threads : " << numThreads << std::endl;
  if(argc == 5)
    numThreads = atoi(argv[4]);
  std::cout << "User set TBB threads : " << numThreads << std::endl;
    //make sure the task_scheduler_init object is in scope when running sth w/ TBB
  tbb::task_scheduler_init init(numThreads);
#endif

  using DeviceAdapter = VTKM_DEFAULT_DEVICE_ADAPTER_TAG;

  if(!real)
  {
    // Points, and other parameters are already populated in
    // the method ParseAndPopulate.
    PointMergeNearestNeighbor(points, delta, bounds, globalMap, DeviceAdapter());
  }
  else
  {
    vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
    vtkm::cont::DataSet toProcess = reader.ReadDataSet();

    //Populate the required fields.
    bounds = toProcess.GetCoordinateSystem().GetBounds();
    vtkm::cont::ArrayHandleVirtualCoordinates interm = toProcess.GetCoordinateSystem().GetData();
    vtkm::cont::ArrayCopy(interm, points);

    vtkm::cont::Timer<> timer;

    // Run the algorithm
    PointMergeNearestNeighbor(points, delta, bounds, globalMap, DeviceAdapter());

    std::cout << "Time to finish merging points and generate mapping : "
              << timer.GetElapsedTime() << std::endl;
    timer.Reset();

    std::cout << "After processing, number of points "
              << points.GetNumberOfValues() << std::endl;

    //Ammend Connectivity
    vtkm::cont::DataSet ammendedDataset;
    AmmendConnectivity(toProcess, globalMap, points, ammendedDataset, DeviceAdapter());

    std::cout << "Time to build dataset with merged points : " << timer.GetElapsedTime()
              << std::endl;

    // Print merge stats
    std::cout << "Output cells : "
              << ammendedDataset.GetCellSet().GetNumberOfCells() << std::endl;
    std::cout << "Output points : "
              << ammendedDataset.GetCoordinateSystem().GetData().GetNumberOfValues()
              << std::endl;

    // Do a Quality check on the generated dataset.
    // AnalyzeMeshIssues(ammendedDataset, DeviceAdapter());
    //std::cout << "Writing output dataset" << std::endl;
    //vtkm::io::writer::VTKDataSetWriter oWriter("output.vtk");
    //oWriter.WriteDataSet(ammendedDataset);
  }
  return 0;
}
