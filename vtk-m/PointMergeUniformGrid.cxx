#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <limits>

#ifdef _BUILDING_TBB_
#include <tbb/task_scheduler_init.h>
#endif

#include <vtkm/BinaryOperators.h>
#include <vtkm/Bounds.h>
#include <vtkm/Types.h>
#include <vtkm/Range.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleExtractComponent.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/cont/Timer.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/Keys.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/DispatcherReduceByKey.h>

#include "PointMergeWorklets.h"
#include "Utilities.hxx"
#include "DefineTypes.h"

using Point = vtkm::Vec<vtkm::FloatDefault, 3>;
using BinIndexHandle = vtkm::cont::ArrayHandle<BinIndex>;
using IdHandle = vtkm::cont::ArrayHandle<vtkm::Id>;

void GetOrigins(std::vector<vtkm::Bounds>& boundsVec,
                vtkm::FloatDefault delta,
                const vtkm::Bounds& originalBounds)
{
  if(delta == 0)
  {
    boundsVec.push_back(originalBounds);
    return;
  }

  vtkm::FloatDefault hdelta = delta / static_cast<vtkm::FloatDefault>(2.0f);
  // At this point bounds contains the range over which the points are spread.
  // We need to cater these bounds for uniform grid with delta spacing.
  vtkm::Bounds bounds = vtkm::Bounds(
    vtkm::Range(originalBounds.X.Min - delta, vtkm::Ceil(originalBounds.X.Max / delta)*delta + delta),
    vtkm::Range(originalBounds.Y.Min - delta, vtkm::Ceil(originalBounds.Y.Max / delta)*delta + delta),
    vtkm::Range(originalBounds.Z.Min - delta, vtkm::Ceil(originalBounds.Z.Max / delta)*delta + delta)
  );

  // The baseline bounds
  boundsVec.push_back(bounds);

  // Shift by half across faces (X, Y, and Z directions)
  boundsVec.push_back(
    vtkm::Bounds(vtkm::Range(bounds.X.Min - hdelta, bounds.X.Max - hdelta), bounds.Y, bounds.Z)
  );
  boundsVec.push_back(
    vtkm::Bounds(bounds.X, vtkm::Range(bounds.Y.Min - hdelta, bounds.Y.Max - hdelta), bounds.Z)
  );
  boundsVec.push_back(
    vtkm::Bounds(bounds.X, bounds.Y, vtkm::Range(bounds.Z.Min - hdelta, bounds.Z.Max - hdelta))
  );

  // Shift by half across edges (X/Y, X/Z, Y/Z directions)
  boundsVec.push_back(
    vtkm::Bounds(vtkm::Range(bounds.X.Min - hdelta, bounds.X.Max - hdelta),
                 vtkm::Range(bounds.Y.Min - hdelta, bounds.Y.Max - hdelta),
                 bounds.Z)
  );
  boundsVec.push_back(
    vtkm::Bounds(vtkm::Range(bounds.X.Min - hdelta, bounds.X.Max - hdelta),
                 bounds.Y,
                 vtkm::Range(bounds.Z.Min - hdelta, bounds.Z.Max - hdelta))
  );
  boundsVec.push_back(
    vtkm::Bounds(bounds.X,
                 vtkm::Range(bounds.Y.Min - hdelta, bounds.Y.Max - hdelta),
                 vtkm::Range(bounds.Z.Min - hdelta, bounds.Z.Max - hdelta))
  );

  // Shift by half diagonally
  boundsVec.push_back(
    vtkm::Bounds(vtkm::Range(bounds.X.Min - hdelta, bounds.X.Max - hdelta),
                 vtkm::Range(bounds.Y.Min - hdelta, bounds.Y.Max - hdelta),
                 vtkm::Range(bounds.Z.Min - hdelta, bounds.Z.Max - hdelta))
  );
}

template <typename DeviceAdapter>
void PointMergeUniformGrid(vtkm::cont::ArrayHandle<Point>& points,
                           vtkm::FloatDefault delta,
                           const vtkm::Bounds& originalBounds,
                           IdHandle& globalMap,
                           DeviceAdapter)
{
  using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;

  assert(points.GetNumberOfValues() != 0);
  std::cout << "Number of Points : " << points.GetNumberOfValues()
            << std::endl;

  // This needs to be a explicit template method that will populate
  // the dimensions for the grid. based on the type provided.
  vtkm::Vec<BinIndex, 3> dimensions;
  GetGridDimensions(originalBounds, delta, dimensions);

  // Generate the origins for testing iteratively.
  // Shift along axes, along edges, along diagonal.
  // In all we should have 8 origins.
  std::vector<vtkm::Bounds> boundsVec;
  GetOrigins(boundsVec, delta, originalBounds);

  // Local map of points that merge together.
  IdHandle localMap;
  {
    // Populate the local and global map to initial values.
    vtkm::cont::ArrayHandleIndex indices(points.GetNumberOfValues());
    DeviceAlgorithm::Copy(indices, localMap);
    DeviceAlgorithm::Copy(indices, globalMap);
  }

  vtkm::cont::Timer<> timer;

  // Perform iterations.
  for(size_t iter = 0; iter < boundsVec.size(); iter++)
  {
    vtkm::Bounds bounds = boundsVec.at(iter);
    BinIndexHandle binIds;
    // Bin points into bins that correspond to the cells the points belong to.
    using BinningDispatcher =
      typename vtkm::worklet::DispatcherMapField<pointmerge::PointToCellId<BinIndex>, DeviceAdapter>;
    pointmerge::PointToCellId<BinIndex> pointToCellWorklet(dimensions, bounds);
    BinningDispatcher pointToCellDispatch(pointToCellWorklet);
    pointToCellDispatch.Invoke(points, binIds);

    std::cout << "Time to bin points into cells : " << timer.GetElapsedTime()
            << std::endl;
    timer.Reset();

    vtkm::cont::ArrayHandleIndex pointIndices(points.GetNumberOfValues());
    // Based on binIds, find nearest neighbors. There could be multiple points
    // in the output that cannot be merged any more. This happens when delta is
    // smaller than the grid spacing.
    // These need to exist seperatedly. Also we need the mimimum index of points
    // that are considered neighbor.
    IdHandle neighbors;
    vtkm::worklet::Keys<BinIndex> binKeys(binIds, DeviceAdapter());
    pointmerge::BinsToNeighbors binsToNeighborsWorklet(delta);
    using BinsToNeighborsDispatcher
      = typename vtkm::worklet::DispatcherReduceByKey<pointmerge::BinsToNeighbors, DeviceAdapter>;
    BinsToNeighborsDispatcher binsToNeighborsDispatcher(binsToNeighborsWorklet);
    binsToNeighborsDispatcher.Invoke(binKeys, pointIndices, points, neighbors);

    std::cout << "Time to collect neighbors from bins : " << timer.GetElapsedTime()
            << std::endl;
    timer.Reset();

    // Check if indices and neighbors are the same.
    // If they are, the merge has converged.
    IdHandle comparison;
    comparison.Allocate(1);
    {
      auto comparePortal = comparison.GetPortalControl();
      comparePortal.Set(0, 0);
    }
    using ComparisonDispatcher =
       vtkm::worklet::DispatcherMapField<pointmerge::CompareWorklet, DeviceAdapter>;
    ComparisonDispatcher().Invoke(neighbors, comparison);

    std::cout << "Time to make decision for next iteration : " << timer.GetElapsedTime()
            << std::endl;
    timer.Reset();
    {
      auto comparePortal = comparison.GetPortalConstControl();
      if(comparePortal.Get(0) == 0)
        continue; 
    }
    // Auxiliary arrays to complete reduce by key properly.
    IdHandle updatedMap;
    vtkm::cont::ArrayHandle<Point> mergedPoints;
    // The call to reduce by key does 3 things
    // 1. Reduces the points that are neighbors.
    // 2. Updates the global map with the merged neighbors.
    // 3. Reduces the local map for the next round.
    vtkm::worklet::DispatcherReduceByKey<pointmerge::PerformMergeForRound, DeviceAdapter> dispatcher;
    vtkm::worklet::Keys<vtkm::Id> neighborKeys(neighbors, DeviceAdapter());
    dispatcher.Invoke(neighborKeys, pointIndices, points, localMap, globalMap, mergedPoints, updatedMap);

    vtkm::cont::ArrayHandleIndex indices(globalMap.GetNumberOfValues());
    vtkm::worklet::DispatcherMapField<pointmerge::FixNeighborhoodWorklet, DeviceAdapter> fixDispatcher;
    fixDispatcher.Invoke(indices, globalMap);

    std::cout << "Time to merge points and update maps : " << timer.GetElapsedTime()
            << std::endl;
    timer.Reset();

    // Update points and local map for the next round.
    DeviceAlgorithm::Copy(mergedPoints, points);
    DeviceAlgorithm::Copy(updatedMap, localMap);

    std::cout << "Time to ready data for next iteration : " << timer.GetElapsedTime()
            << std::endl;
    timer.Reset();
  }
  timer.Reset();
  CondenseGlobalMapIndices(globalMap, DeviceAdapter());
  std::cout << "Time to update global map : " << timer.GetElapsedTime()
            << std::endl;
}

int main(int argc, char** argv)
{
  vtkm::cont::ArrayHandle<Point> points;
  IdHandle globalMap;
  vtkm::FloatDefault delta;
  vtkm::Bounds bounds;
  bool real = false;
  std::string datafile;

  ParseAndPopulate(argc, argv, points, delta, bounds, real, datafile);

#ifdef _BUILDING_TBB_
  int numThreads = tbb::task_scheduler_init::default_num_threads();
  std::cout << "Default TBB threads : " << numThreads << std::endl;
  if(argc == 5)
    numThreads = atoi(argv[4]);
  std::cout << "User set TBB threads : " << numThreads << std::endl;
    //make sure the task_scheduler_init object is in scope when running sth w/ TBB
  tbb::task_scheduler_init init(numThreads);
#endif

  using DeviceAdapter = VTKM_DEFAULT_DEVICE_ADAPTER_TAG;

  if(!real)
  {
    // Points, and other parameters are already populated in
    // the method ParseAndPopulate.
    PointMergeUniformGrid(points, delta, bounds, globalMap, DeviceAdapter());
  }
  else
  {
    vtkm::io::reader::VTKDataSetReader reader(datafile.c_str());
    vtkm::cont::DataSet toProcess = reader.ReadDataSet();

    //Populate the required fields.
    bounds = toProcess.GetCoordinateSystem().GetBounds();
    vtkm::cont::ArrayHandleVirtualCoordinates interm = toProcess.GetCoordinateSystem().GetData();
    vtkm::cont::ArrayCopy(interm, points);

    vtkm::cont::Timer<> timer;

    // Run the algorithm
    PointMergeUniformGrid(points, delta, bounds, globalMap, DeviceAdapter());

    std::cout << "Time to finish merging points and generate mapping : "
              << timer.GetElapsedTime() << std::endl;
    timer.Reset();

    std::cout << "After processing, number of points "
              << points.GetNumberOfValues() << std::endl;

    //Ammend Connectivity
    vtkm::cont::DataSet ammendedDataset;
    AmmendConnectivity(toProcess, globalMap, points, ammendedDataset, DeviceAdapter());

    std::cout << "Time to build dataset with merged points : "
              << timer.GetElapsedTime() << std::endl;

    // Print merge stats
    std::cout << "Output cells : "
              << ammendedDataset.GetCellSet().GetNumberOfCells() << std::endl;
    std::cout << "Output points : "
              << ammendedDataset.GetCoordinateSystem().GetData().GetNumberOfValues()
              << std::endl;

    // Do a Quality check on the generated dataset.
    // AnalyzeMeshIssues(ammendedDataset, DeviceAdapter());
    //std::cout << "Writing output dataset" << std::endl;
    //vtkm::io::writer::VTKDataSetWriter oWriter("output.vtk");
    //oWriter.WriteDataSet(ammendedDataset);
  }
  return 0;
}
