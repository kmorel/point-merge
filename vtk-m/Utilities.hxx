#ifndef utilities_hxx
#define utilities_hxx

#include <cmath>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <random>
#include <string>
#include <thread>
#include <vector>
#include <limits>
#include <typeinfo>

#include <vtkm/Bounds.h>
#include <vtkm/Types.h>
#include <vtkm/Range.h>
#include <vtkm/VectorAnalysis.h>

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/ArrayCopy.h>
#include <vtkm/cont/Algorithm.h>
#include <vtkm/cont/CellSetExplicit.h>

#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

#include <vtkm/worklet/DispatcherMapTopology.h>

#include "PointMergeWorklets.h"

using Point = vtkm::Vec<vtkm::FloatDefault, 3>;
using IdHandle = vtkm::cont::ArrayHandle<vtkm::Id>;

template <typename T>
bool IsSafe(T a, T b)
{
  T max = std::numeric_limits<T>::max();
  if (a > max / b) /* `a * b` would overflow */
    return false;
  return true;
}

template <typename T>
bool ProductOverflow(T dimX, T dimY, T dimZ)
{
  bool temp1 = IsSafe(dimX, dimY);
  if(temp1)
  { T res = dimX*dimY; return !IsSafe(res, dimZ); }
  else
    return true;
}

template <typename CellIndex>
void GetGridDimensions(const vtkm::Bounds& bounds,
                       const vtkm::FloatDefault delta,
                       vtkm::Vec<CellIndex, 3>& dimensions)
{
  CellIndex max_cells = std::numeric_limits<CellIndex>::max();
  vtkm::FloatDefault tdelta = 2*delta;
  bool overflow = false;
  CellIndex dimX = static_cast<CellIndex>(vtkm::Floor(bounds.X.Length() / tdelta));
  CellIndex dimY = static_cast<CellIndex>(vtkm::Floor(bounds.Y.Length() / tdelta));
  CellIndex dimZ = static_cast<CellIndex>(vtkm::Floor(bounds.Z.Length() / tdelta));
  // Check for overflow conditions and 0 delta
  if(delta == 0 || ProductOverflow(dimX, dimY, dimZ))
    overflow = true;
  if(overflow)
  {
    CellIndex dimension
      = static_cast<CellIndex>(vtkm::Floor(cbrt((vtkm::FloatDefault)max_cells)));
    dimensions = vtkm::make_Vec(dimension, dimension, dimension);
  }
  else
  {
    dimensions = vtkm::make_Vec(dimX, dimY, dimZ);
  }
  std::cout << "Dimensions for virtual grid : " << dimensions << std::endl;
}

vtkm::FloatDefault Distance(const Point& x, const Point& y)
{
  vtkm::FloatDefault distance =  static_cast<vtkm::FloatDefault>(sqrt( pow(x[0] - y[0], 2)
                                                                     + pow(x[1] - y[1], 2)
                                                                     + pow(x[2] - y[2], 2)));
  return distance;
}

void PrintPointArray(vtkm::cont::ArrayHandle<Point>& pointArray, vtkm::Id numValues = 10)
{
  numValues = std::min(numValues, pointArray.GetNumberOfValues());
  auto portal = pointArray.GetPortalConstControl();
  for(int i = 0; i < numValues; i++)
    std::cout << portal.Get(i) << std::endl;
  std::cout << std::endl;
}

template <typename IdType>
void PrintIdArray(vtkm::cont::ArrayHandle<IdType>& idArray, vtkm::Id numValues = 10)
{
  numValues = std::min(numValues, idArray.GetNumberOfValues());
  auto portal = idArray.GetPortalConstControl();
  std::cout << std::fixed;
  for(size_t i = 0; i < numValues; i++)
    std::cout << portal.Get(i) << "\t";
  std::cout << std::endl;
}

void PrintBruteForce(vtkm::cont::ArrayHandle<Point>& points)
{
  std::cout << std::fixed;
  std::cout << std::setprecision(5);
  auto portal = points.GetPortalConstControl();
  for(int i = 0; i < points.GetNumberOfValues(); i++)
  {
    for(int j = 0; j < points.GetNumberOfValues(); j++)
    {
      if(i == j)
        std::cout << "X" << "\t";
      else
        std::cout << Distance(portal.Get(i), portal.Get(j)) << "\t";
    }
    std::cout << std::endl;
  }
}

void GenerateRandomPoints(vtkm::cont::ArrayHandle<Point>& points,
                          vtkm::Id numberOfPoints,
                          vtkm::Bounds& bounds)
{
  std::vector<Point> pointvec;
  // Random distribution, many points.
  std::default_random_engine rand;
  // The bounds for each axis does not matter as this is user provided
  // for equal extents along all axes
  std::uniform_real_distribution<vtkm::FloatDefault>
    dist(static_cast<vtkm::FloatDefault>(bounds.X.Min),
         static_cast<vtkm::FloatDefault>(bounds.X.Max));

  Point point;
  for (int i = 0; i < numberOfPoints; i++)
  {
    point = vtkm::make_Vec(dist(rand), dist(rand), dist(rand));
    pointvec.push_back(point);
  }

  vtkm::cont::ArrayHandle<Point> interm =  vtkm::cont::make_ArrayHandle(pointvec);
  vtkm::cont::ArrayCopy(interm, points);
}

void GenerateTestPoints(vtkm::cont::ArrayHandle<Point>& points,
                        vtkm::FloatDefault delta,
                        vtkm::Bounds& bounds)
{
  std::vector<Point> pointvec;
  std::default_random_engine rand;
  std::uniform_real_distribution<vtkm::FloatDefault> dir(-1.0f, 1.0f);
  std::uniform_real_distribution<vtkm::FloatDefault> l_dist(0.0f, 1.0f);
  std::uniform_real_distribution<vtkm::FloatDefault> g_dist(1.1f, 2.0f);
  // Start with a point placed in the middle of the Grid.
  // 5, 5, 5.
  Point seed(5.0,5.0,5.0);
  pointvec.push_back(seed);
  // Generate few points closer.
  for(int i = 0; i < 5; i++)
  {
    Point direction = vtkm::make_Vec(dir(rand), dir(rand), dir(rand));
    vtkm::Normalize(direction);
    vtkm::FloatDefault disp = delta*l_dist(rand);
    Point point(seed[0] + disp*direction[0],
                seed[1] + disp*direction[1],
                seed[2] + disp*direction[2]);
    //Point point(5.0,5.0,5.0);
    if(!(bounds.Contains(point)))
      continue;
    pointvec.push_back(point);
  }
  // Generate few points farther.
  for(int i = 0; i < 5; i++)
  {
    Point direction = vtkm::make_Vec(dir(rand), dir(rand), dir(rand));
    vtkm::Normalize(direction);
    vtkm::FloatDefault disp = delta*g_dist(rand);
    Point point(seed[0] + disp*direction[0],
                seed[1] + disp*direction[1],
                seed[2] + disp*direction[2]);
    if(!(bounds.Contains(point)))
      continue;
    pointvec.push_back(point);
  }
  std::cout << "Printing points" << std::endl;
  for(size_t i = 0; i < pointvec.size(); i++)
  {
    std::cout << pointvec[i] << std::endl;
  }
  vtkm::cont::ArrayHandle<Point> interm =  vtkm::cont::make_ArrayHandle(pointvec);
  vtkm::cont::ArrayCopy(interm, points);
  // Print Brute Force to get the idea of which points should
  // have been grouped together by the end of the algorithm.
  PrintBruteForce(points);
}

void ParseAndPopulate(int argc,
                      char** argv,
                      vtkm::cont::ArrayHandle<Point>& points,
                      vtkm::FloatDefault& delta,
                      vtkm::Bounds& bounds,
                      bool& real,
                      std::string& datafile)
{
  if(argc < 4)
  {
    std::cout << "Invalid number of arguments provided" << std::endl;
    std::cout << "Usage : pointmerge <delta> <range start> <range end> <test option> <num points>"
              << std::endl;
    std::cout << "delta : required" << std::endl;
    std::cout << "test option : optional" << std::endl;
    std::cout << "range start : required" << std::endl;
    std::cout << "range end : required" << std::endl;
    std::cout << "num points : optional" << std::endl;
    std::cout << "" << std::endl;
    exit(EXIT_FAILURE);
  }

  // This is required in all cases.
  delta = static_cast<vtkm::FloatDefault>(atof(argv[1]));
  // Option to test.
  vtkm::Id test = static_cast<vtkm::Id>(atoi(argv[2]));

  // For this example the bounding box is the same length along each axis
  vtkm::Range range;
  vtkm::Id numPoints = 10;
  if(test == 1 || test == 2)
  {
    vtkm::FloatDefault rangeMin = static_cast<vtkm::FloatDefault>(atof(argv[3]));
    vtkm::FloatDefault rangeMax = static_cast<vtkm::FloatDefault>(atof(argv[4]));
    range = vtkm::Range(rangeMin, rangeMax);
    bounds = vtkm::Bounds(vtkm::Range(range.Min, range.Max),
                            vtkm::Range(range.Min, range.Max),
                            vtkm::Range(range.Min, range.Max));
   if(argc == 6)
     numPoints = static_cast<vtkm::Id>(atoi(argv[5]));
   if(test == 1)
     GenerateTestPoints(points, delta, bounds);
   else if(test == 2)
     GenerateRandomPoints(points, numPoints, bounds);
  }
  else if(test == 3)
  {
    datafile = std::string(argv[3]);
    real = true;
  }
}

template <typename DeviceAdapter>
void CondenseGlobalMapIndices(IdHandle& globalMap, DeviceAdapter)
{
  using DeviceAlgorithm
    = typename vtkm::cont::DeviceAdapterAlgorithm<DeviceAdapter>;
  // Fix global map such that if there are any indices greater than
  // the number of remining points, they're fixed so that we have the
  // maximum index that corresponds to the number of points.
  IdHandle unique, copy;
  DeviceAlgorithm::Copy(globalMap, unique);
  DeviceAlgorithm::Copy(globalMap, copy);
  DeviceAlgorithm::Sort(unique);
  DeviceAlgorithm::Unique(unique);
  std::cout << "Number of unique indices to be assigned to points : "
            << unique.GetNumberOfValues() << std::endl;
  DeviceAlgorithm::LowerBounds(unique, copy, globalMap);
}

template <typename DeviceAdapter>
void AmmendConnectivity(vtkm::cont::DataSet& inputDataset,
                        vtkm::cont::ArrayHandle<vtkm::Id>& ammendMap,
                        vtkm::cont::ArrayHandle<Point>& points,
                        vtkm::cont::DataSet& outputDataset,
                        DeviceAdapter)
{
  vtkm::cont::DynamicCellSet cellset = inputDataset.GetCellSet();

  if(cellset.IsType<vtkm::cont::CellSetStructured<2>>()
     || cellset.IsType<vtkm::cont::CellSetStructured<3>>())
  {
    outputDataset = inputDataset;
    return;
  }
  else if(cellset.IsType<vtkm::cont::CellSetSingleType<>>())
  {
    vtkm::cont::CellSetSingleType<> forInput = cellset.Cast<vtkm::cont::CellSetSingleType<>>();
    auto connectivity
      = forInput.GetConnectivityArray(vtkm::TopologyElementTagPoint(),
                                      vtkm::TopologyElementTagCell());
    /*std::cout << "Number of connectivity elements : " << connectivity.GetNumberOfValues()
              << std::endl;*/

    vtkm::cont::ArrayHandle<vtkm::Id> fixedConnectivity;
    //Worklet to Ammend
    vtkm::worklet::DispatcherMapField<pointmerge::FixConnectivity, DeviceAdapter> dispatcher;
    dispatcher.Invoke(connectivity, fixedConnectivity, ammendMap);
    // Add new CellSet to the output dataset
    vtkm::cont::CellSetSingleType<> forOutput;
    forOutput.Fill(forInput.GetNumberOfCells(), forInput.GetCellShape(0), 3, fixedConnectivity);
    outputDataset.AddCellSet(forOutput);
  }
  else
  {
    vtkm::cont::CellSetExplicit<> forInput = cellset.Cast<vtkm::cont::CellSetExplicit<>>();
    auto connectivity
      = forInput.GetConnectivityArray(vtkm::TopologyElementTagPoint(),
                                      vtkm::TopologyElementTagCell());
    /*std::cout << "Number of connectivity elements : " << connectivity.GetNumberOfValues()
              << std::endl;*/

    vtkm::cont::ArrayHandle<vtkm::Id> fixedConnectivity;
    //Worklet to Ammend
    vtkm::worklet::DispatcherMapField<pointmerge::FixConnectivity, DeviceAdapter> dispatcher;
    dispatcher.Invoke(connectivity, fixedConnectivity, ammendMap);
    vtkm::cont::ArrayHandle<vtkm::UInt8> shapes;
    auto _shapes = forInput.GetShapesArray(vtkm::TopologyElementTagPoint(),
                                           vtkm::TopologyElementTagCell());
    vtkm::cont::Algorithm::Copy(_shapes, shapes);
    auto numIndices = forInput.GetNumIndicesArray(vtkm::TopologyElementTagPoint(),
                                                  vtkm::TopologyElementTagCell());
    // Add new CellSet to the output dataset
    vtkm::cont::CellSetExplicit<> forOutput;
    forOutput.Fill(forInput.GetNumberOfCells(), shapes, numIndices, fixedConnectivity);
    outputDataset.AddCellSet(forOutput);
  }

  // Add new Coordinate System to the dataset
  vtkm::cont::CoordinateSystem coords("ammended", points);
  outputDataset.AddCoordinateSystem(coords);
}

template <typename DeviceAdapter>
void AnalyzeMeshIssues(vtkm::cont::DataSet& dataset, DeviceAdapter)
{
  vtkm::worklet::DispatcherMapTopology<pointmerge::DegenerateTriangles, DeviceAdapter> degenerate;
  degenerate.Invoke(dataset.GetCellSet());
  vtkm::Id numVertices = dataset.GetCoordinateSystem().GetData().GetNumberOfValues();
  pointmerge::BadVertexIndex badVertexWorklet(numVertices);
  vtkm::worklet::DispatcherMapTopology<pointmerge::BadVertexIndex, DeviceAdapter> badVertex(badVertexWorklet);
  badVertex.Invoke(dataset.GetCellSet());
}

#endif
