#ifndef pointmergeworklets_h
#define pointmergeworklets_h

#include <vtkm/Types.h>
#include <vtkm/VectorAnalysis.h>

#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/worklet/WorkletMapTopology.h>
#include <vtkm/worklet/WorkletReduceByKey.h>

namespace pointmerge
{

// This worklet is called in the case of nearest neighbor approach,
// got each input point, this worklet calls the Point Locator Self
// Join to find the closest neighbor.
class GetNearestNeighbor : public vtkm::worklet::WorkletMapField
{
public:
  VTKM_CONT
  GetNearestNeighbor(vtkm::FloatDefault _delta)
  : deltasqr(_delta*_delta)
  {}

  using ControlSignature = void(FieldIn<> point,
                                ExecObject locator,
                                FieldOut<> neighbor,
                                FieldOut<> distance);

  using ExecutionSignature = void(_1, WorkIndex, _2, _3, _4);

  template <typename PointType, typename Locator>
  VTKM_EXEC void operator()(const PointType& vtkmNotUsed(point),
                            const vtkm::Id& index,
                            const Locator& locator,
                            vtkm::Id& neighbor,
                            vtkm::FloatDefault& distance) const
  {
    // In case of self join, instead of point
    // we need the index, so that we can discard itself as
    // the closest neighbor.
    locator->FindNearestNeighbor(index, neighbor, distance);
    if(distance > deltasqr)
      neighbor = index;
    // Always return the smaller of the two values.
    neighbor = (neighbor >= index) ? index : neighbor;
  }

private:
  vtkm::FloatDefault deltasqr;
};

class CompareWorklet : public vtkm::worklet::WorkletMapField
{
public:
  typedef void ControlSignature(FieldIn<>,
                                WholeArrayInOut<>);

  typedef void ExecutionSignature(WorkIndex, _1, _2);

  template <typename T,
            typename ArrayPortalType>
  VTKM_EXEC void operator()(T val1,
                            T val2,
                            ArrayPortalType& result) const
  {
    if(val1 ^ val2)
    {
      result.Set(0, 1);
    }
  }
};

// Map Points to Cells, here Cell Ids can be configured to be
// and single numeric data type (vtkm::Int32, vtkm::Int64, etc)
template <typename CellIndexType>
class PointToCellId : public vtkm::worklet::WorkletMapField
{
public:
  using ControlSignature = void(FieldIn<> point,
                                FieldOut<> cellId);
  using ExecutionSignature = void(_1, WorkIndex, _2);

  template <typename Point>
  VTKM_EXEC void operator()(const Point& point,
                            const vtkm::Id& vtkmNotUsed(index),
                            CellIndexType& cellId) const
  {
    vtkm::Vec<vtkm::FloatDefault, 3> relative;
    relative[0] = static_cast<vtkm::FloatDefault>((point[0] - Bounds.X.Min) * Scale[0]);
    relative[1] = static_cast<vtkm::FloatDefault>((point[1] - Bounds.Y.Min) * Scale[1]);
    relative[2] = static_cast<vtkm::FloatDefault>((point[2] - Bounds.Z.Min) * Scale[2]);
    vtkm::Vec<CellIndexType, 3> logicalCell;
    logicalCell[0] = static_cast<CellIndexType>(vtkm::Floor(relative[0]));
    logicalCell[1] = static_cast<CellIndexType>(vtkm::Floor(relative[1]));
    logicalCell[2] = static_cast<CellIndexType>(vtkm::Floor(relative[2]));
    cellId = logicalCell[2] * PlaneSize + logicalCell[1] * RowSize + logicalCell[0];
    //if(cellId < 0)
    //  std::cout << "Zero!!" << point << std::endl;
  }

  VTKM_CONT
  PointToCellId(vtkm::Vec<CellIndexType, 3>& dims,
                vtkm::Bounds& bounds)
    : Bounds(bounds)
    , Dims(dims)
  {
    Scale[0]
      = static_cast<vtkm::FloatDefault>((dims[0] - 1.0l) / (bounds.X.Max - bounds.X.Min));
    Scale[1]
      = static_cast<vtkm::FloatDefault>((dims[1] - 1.0l) / (bounds.Y.Max - bounds.Y.Min));
    Scale[2]
      = static_cast<vtkm::FloatDefault>((dims[2] - 1.0l) / (bounds.Z.Max - bounds.Z.Min));
    PlaneSize = dims[0] * dims[1];
    RowSize = dims[0];
  }

private:
  vtkm::Bounds Bounds;
  vtkm::Vec<vtkm::FloatDefault, 3> Scale;
  vtkm::Vec<CellIndexType, 3> Dims;
  CellIndexType PlaneSize;
  CellIndexType RowSize;
};

// In successive iterations when points get merged into each other,
// the local map has no ability to update points that were previously
// mapped to some point being merged in the current iteration.
// This worklet checks to see if the index that the current point is
// merge into has not changed again, if it has it sets it's own mapped
// index to
class FixNeighborhoodWorklet : public vtkm::worklet::WorkletMapField
{
public:
  using ControlSignature = void(FieldIn<> index,
                                WholeArrayInOut<> toFix);

  using ExecutionSignature = void(_1, _2);

  template <typename MapPortalType>
  VTKM_EXEC void operator()(const vtkm::Id& index,
                            MapPortalType& toFix) const
  {
    auto nearest = toFix.Get(index);
    auto value_at_nearest = toFix.Get(nearest);
    if(value_at_nearest != nearest)
    {
      toFix.Set(index, value_at_nearest);
    }
  }
};

// Change the old vertex indices making up a triangle to new
// indices.
struct FixConnectivity : public vtkm::worklet::WorkletMapField
{
public:
  using ControlSignature = void(FieldIn<>,
                                FieldOut<>,
                                WholeArrayIn<>);

  using ExecutionSignature = void(_1, _2, _3);

  template<typename AmmendMapConstType>
  VTKM_EXEC void operator()(const vtkm::Id& oldConnectivity,
                            vtkm::Id& newConnectivity,
                            const AmmendMapConstType ammendMap) const
  {
    newConnectivity = ammendMap.Get(oldConnectivity);
  }
};

// Since the output of the Marching Cubes in this case should only
// be trinagles, this is to check if we have any degenerate triangles.
struct DegenerateTriangles : public vtkm::worklet::WorkletMapPointToCell
{
public:
  using ControlSignature = void(CellSetIn);

  using ExecutionSignature = void(PointCount,
                                  PointIndices);
  template <typename PointIndicesType>
  VTKM_EXEC void operator()(vtkm::IdComponent counts,
                            PointIndicesType points) const
  {
    assert (counts == 3);
    assert (points.GetNumberOfComponents() == 3);
    if(points[0] == points[1]
       || points[1] == points[2]
       || points[2] == points[0])
    {
      std::cout << "Found Error Cell" << std::endl;
    }
  }
};

// One error we can run into when the indices are not mapped
// properly is that the vertex index can be greater than the
// number of vertices.
struct BadVertexIndex : public vtkm::worklet::WorkletMapPointToCell
{
public:
  BadVertexIndex(vtkm::Id numVertices)
  : NumVertices(numVertices)
  {}

  using ControlSignature = void(CellSetIn);

  using ExecutionSignature = void(PointCount,
                                  PointIndices);
  template <typename PointIndicesType>
  VTKM_EXEC void operator()(vtkm::IdComponent counts,
                            PointIndicesType points) const
  {
   for(int i = 0; i < counts; i++)
   {
     if(points[i] >= NumVertices)
       std::cout << "Found Error Vertex" << std::endl;
   }
 }
private:
  vtkm::Id NumVertices;
};


// When working with the virtual grid apprpach for, we do not know
// which points are neighbors are the distances between them.
// This worklet is meant to find distince neighbors in a certain
// cell and return the corresponding nearest neighbor with the
// smallest index for each of the points mapped to the cell.
class BinsToNeighbors : public vtkm::worklet::WorkletReduceByKey
{
public :
  BinsToNeighbors(vtkm::FloatDefault delta)
    : tolerance(delta*delta)
  {}

  using ControlSignature = void(KeysIn keys,
                                ValuesIn<> indices,
                                ValuesIn<> points,
                                ValuesOut<> neighbors);

  using ExecutionSignature = void(_1, _2, _3, _4);

  template <typename IndexVecInType,
            typename PointVecType,
            typename IndexVecOutType>
  VTKM_EXEC void operator()(const vtkm::Id& vtkmNotUsed(key),
                            const IndexVecInType& indices,
                            const PointVecType& points,
                            IndexVecOutType& neighbors) const
  {
    // For each point, find the closest point with minimum index.
    // If the point is within delta, they should be merged.
    // If the points do not occur in delta, they should be left
    // alone. **Debug : 781133
    // -----------------------------------------------------------
    // Note: indices are not sorted, so need additional processing
    //       to put the lowest indices in neighbors
    using Point = typename PointVecType::ComponentType;
    using Index = typename IndexVecInType::ComponentType;

    vtkm::IdComponent numPoints = indices.GetNumberOfComponents();

    for(vtkm::IdComponent pIndex = 0; pIndex < numPoints; ++pIndex)
      neighbors[pIndex] = indices[pIndex];

    for(vtkm::IdComponent pIndex = 0; pIndex < numPoints; ++pIndex)
    {
      Point x = points[pIndex];
      vtkm::FloatDefault current = vtkm::Infinity<vtkm::FloatDefault>();
      for(vtkm::IdComponent nIndex = pIndex + 1; nIndex < numPoints ; ++nIndex)
      {
        Point y = points[nIndex];
        vtkm::FloatDefault distance = vtkm::MagnitudeSquared(x - y);
        if(distance <= tolerance && distance <= current)
        {
          // Replace the neighbor as the current point
          // only if it's index is smaller than the last
          // found neighbor.
          Index minIndex
            = vtkm::Min((Index)indices[nIndex], (Index)neighbors[pIndex]);
          neighbors[pIndex] = minIndex;
          neighbors[nIndex] = minIndex;
        }
      }
    }
  }
private:
  vtkm::FloatDefault tolerance;
};

// After we have found the nearest neighbors for each of the input
// points, we need to merge them together(average of all neighbors)
// We also need to maintain and update the map which stores which
// input points are merged into which output points, which may be
// needed to ammend connectivity of cells.
class PerformMergeForRound : public vtkm::worklet::WorkletReduceByKey
{
public:
  using ControlSignature = void(KeysIn keys,
                                ValuesIn<> indices,
                                ValuesIn<> points,
                                WholeArrayIn<> localMapPortal,
                                WholeArrayInOut<> globalMapPortal,
                                ReducedValuesOut<> reducedPoint,
                                ReducedValuesOut<> reducedLocalMap);

  using ExecutionSignature = void(_1, _2, _3, _4, _5 ,_6, _7);

  using ScatterType = vtkm::worklet::ScatterIdentity;

  template <typename IndexVecType,
            typename PointVecType,
            typename MapPortalConstType,
            typename MapPortalType,
            typename PointType,
            typename MapType>
  VTKM_EXEC void operator()(const vtkm::Id& vtkmNotUsed(key),
                            const IndexVecType& indices,
                            const PointVecType& points,
                            MapPortalConstType& localMapPortal,
                            MapPortalType& globalMapPortal,
                            PointType& reducedPoint,
                            MapType& reducedLocalMap) const
  {
    // This worklet has two objectives
    // 1. To merge all the points together that belong to
    //    the same neighborhood
    // 2. To update the maps that keep track of which points
    //    are being merged into which point.
    // ---------------------------------------------------
    // Perform the complete scatter in this worklet, from the local
    // map to the global map since we have all the indices here.
    using Index = typename IndexVecType::ComponentType;

    // Average Points and store result in reducedPoint.
    // Also find the minimum index here that represents
    // the first occurance of the neighbor.
    Index sourceIndex = indices[0];
    PointType sum = points[0];
    auto numPoints = points.GetNumberOfComponents();
    for(vtkm::IdComponent index = 1; index < numPoints; ++index)
    {
      PointType point = points[index];
      sum = sum + point;
      if(indices[index] < sourceIndex)
        sourceIndex = indices[index];
    }
    reducedPoint = sum / static_cast<PointType>(numPoints);

    // Copy the smallest neighbor to all the other locations
    // in the global map from the local map.
    // The local map stores the indices the smallest neighbor
    // is to be copied into the global map.
    MapType smallestNeighbor = localMapPortal.Get(sourceIndex);
    for(vtkm::IdComponent neighbor = 0; neighbor < indices.GetNumberOfComponents(); ++neighbor)
    {
      Index destIndex = localMapPortal.Get(indices[neighbor]);
      globalMapPortal.Set(destIndex, smallestNeighbor);
    }
    reducedLocalMap = smallestNeighbor;
  }
};

} //namespace pointmerge

#endif
