dataset <- read.table("~/repositories/point-merge/forPlots/fishtank", header=FALSE, sep=",")
dataset_pinned <- read.table("~/repositories/point-merge/forPlots/fishtank_pin", header=FALSE, sep=",")

pointloc <- data.frame(x=dataset$V1,y=dataset$V2,type="Point Locator")
pointloc_pin <- data.frame(x=dataset_pinned$V1,y=dataset_pinned$V2,type="Point Locator Pinned")
uniform <- data.frame(x=dataset$V1,y=dataset$V3,type="Uniform Binning")
uniform_pin <- data.frame(x=dataset_pinned$V1,y=dataset_pinned$V3,type="Uniform Binning Pinned")
uniform64 <- data.frame(x=dataset$V1,y=dataset$V4,type="Uniform Binning 64")
uniform64_pin <- data.frame(x=dataset_pinned$V1,y=dataset_pinned$V4,type="Uniform Binning 64 Pinned")
df <- rbind(pointloc, pointloc_pin, uniform, uniform_pin, uniform64, uniform64_pin)

library(ggplot2)

ggplot(df, aes(x = x, y = y)) + 
  geom_line(aes(x,y,colour=type), size=1) + 
  xlab("Number of Threads") + 
  ylab("Speed Up") +
  scale_x_discrete(limits=c(6,12,24,48,96)) + 
  guides(color=guide_legend(title="Algorithm"))