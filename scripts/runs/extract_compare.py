import sys
import os
import os.path
import numpy
import pandas

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping" in line :
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["nearest", "uniform32", "uniform64"]
parenvs = ["serial", "tbb", "cuda"]
tolerances = ["", "0"]
datasets = ["fusion", "thermal hydraulics", "supernova"]

tests_per_thread = 5;

toltimes = {}
for tolerance in tolerances:
  algotimes = {}
  for algorithm in algorithms:
    times = {}
    for parenv in parenvs:
      filename = "%s_%s" %(algorithm, parenv)
      if tolerance is not "":
        filename = "%s_%s" %(filename, tolerance)
      #print "Trying to process file %s" %filename
      if os.path.isfile(filename) and os.access(filename, os.R_OK):
        data = open(filename, "r")
        times[parenv] = GetTimingsFromFile(data)
        data.close()
    algotimes[algorithm] = times
  toltimes[tolerance] = algotimes

outfile = "timings.csv"
output = open(outfile, "w")
header = "tolerance,algorithm,datasets,parenv,time\n"
output.write(header);

for tolerance, algotimes in toltimes.iteritems():
  tol = tolerance
  if tol is "":
    tol = "0.0001"
  for algorithm, partimes in algotimes.iteritems():
    for parenv, times in partimes.iteritems():
      offset = 0
      for dataset in datasets:
        if parenv is "tbb":
          tbboffset = offset + 20 
          time = numpy.amin(times[tbboffset:tbboffset+5])
          towrite = "%s,%s,%s,%s,%f" %(tol, algorithm, dataset, parenv, time)
          output.write("%s\n" %towrite)
          offset += 30
        else:
          time = numpy.amin(times[offset:offset+5])
          towrite =  "%s,%s,%s,%s,%f" %(tol, algorithm, dataset, parenv, time)
          output.write("%s\n" %towrite)
          offset +=5
output.close()

data = pandas.read_csv(outfile)        
comparison = data.pivot_table(values='time', index=['tolerance', 'datasets', 'parenv'], columns='algorithm')
print comparison
  
#output = open(outputfile, "w")
#header = "dataset, threads, normal, normal_rate,pinned, pinned_rate\n"
#output.write(header);
#offset_start = 0;
#for dataset in datasets:
#  for n_threads in threads:
#    normal = numpy.amin(nor_times[offset_start:offset_start+5])
#    offset_start += 5
#    towrite =  "%s, %d, %f, %f" %(dataset, n_threads, normal, (normal / float(points.get(dataset))))
#    if not len(pin_times) == 0 and not n_threads == 96:
#      pinned = numpy.amin(nor_times[offset_start:offset_start+5])
#      towrite = "%s, %f, %f\n" %(towrite, pinned, (pinned / float(points.get(dataset))))
#    else:
#      towrite = "%s\n" %(towrite)
#    output.write(towrite)
