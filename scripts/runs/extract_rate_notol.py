import os
import os.path
import numpy
import pandas
import itertools
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping" in line :
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk_parallel","nearest_tbb_0","uniform32_tbb_0", "uniform64_tbb_0"]
legands = ["VTK", "Point Locator", "Virtual Grid (32-bit)", "Virtual Grid (64-bit)"]
threads = [6, 12, 24, 36, 48, 96]
index = [0, 1, 2, 3, 4]
datasets = ["fusion", "thermal hydraulics", "supernova"]
points = {"fusion" : 4125540, "thermal hydraulics" : 15686430, "supernova" : 24493224}
colors = ["gold", "green", "steelblue", "darkred"]
algocolors = dict(zip(algorithms, colors))
algolegands = dict(zip(algorithms, legands))

def GetAlgoPlotStyle(label):
  global algocolors
  style = {}
  for algo in algorithms:
    if algo in label:
      style["stroke"] = algocolors.get(algo)
  return style

algotimes = {}
for algorithm in algorithms:
  filename = algorithm
  if os.path.isfile(filename) and os.access(filename, os.R_OK):
    data = open(filename, "r")
    times = GetTimingsFromFile(data)
    data.close()
    algotimes[filename] = times      

outfile = "scaling.csv"
output = open(outfile, "w")
header = "algorithm,dataset,threads,time,rate\n"
output.write(header);
offset_start = 0;
for algorithm, times in algotimes.iteritems():
  offset = 0
  for dataset in datasets:
    for n_threads in threads:
      time = 0.
      if "pin" in algorithm and n_threads == 96:
        time = numpy.nan
      else:      
        time = numpy.amin(times[offset:offset+5])  
        offset += 5
      numpoints = points.get(dataset)
      output.write("%s,%s,%s,%f,%f\n" %(algorithm, dataset, n_threads, time, float(numpoints / time)))
output.close()

data = pandas.read_csv(outfile)
scaling = data.pivot_table(values='time', columns=['algorithm', 'dataset'], index='threads')

#legend = {}
for dataset in datasets:
  print "Plot for %s" %dataset
  canvas = toyplot.Canvas('3.5in', '2.6in')
  axes = canvas.cartesian(xlabel = 'Number of available CPU cores', \
                          ylabel = 'Processing Rate (*1M points/sec)', \
                         )
  for algorithm in algorithms:
    data = scaling[algorithm, dataset]
    numpoints = points.get(dataset)
    data = numpoints / data
    # We want points processes per second
    data = data / 1000000
    x = index
    y = numpy.delete(numpy.array(data), 3, 0)
    plotstyle = GetAlgoPlotStyle(algorithm)
    axes.plot(x, y, style=plotstyle)
    text = algolegands.get(algorithm)
    axes._text_colors = itertools.cycle([plotstyle.get("stroke")])      
    axes.text(x[-2], y[-2], text, style={"text-anchor":"start", \
                                         "-toyplot-anchor-shift":"2px", \
                                         "font-size":"10px", \
                                         "-toyplot-vertical-align":"bottom"})
  axes.y.domain.min = 0
  axes.y.domain.max = 40
  axes.x.ticks.locator = \
    toyplot.locator.Explicit(index, numpy.delete(threads, 3, 0))
  toyplot.pdf.render(canvas, "%s_notol.pdf" %dataset.replace(" ", ""))
