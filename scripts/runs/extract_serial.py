import os
import os.path
import numpy
import pandas
import toyplot
import toyplot.pdf

def GetTimingsFromFile(data):
  times = []
  for line in data:
    if "Time to finish merging points and generate mapping" in line :
      time = float((line.split(":")[1]).strip())
      times.append(time)
  return times

algorithms = ["vtk", "nearest","uniform32", "uniform64"]
extensions = ["serial", "serial_0"]
datasets = ["fusion", "thermal hydraulics", "supernova"]

algotimes = {}
for algorithm in algorithms:
  for extension in extensions :
    filename = "%s_%s" %(algorithm, extension)
    if os.path.isfile(filename) and os.access(filename, os.R_OK):
      data = open(filename, "r")
      times = GetTimingsFromFile(data)
      data.close()
      algotimes[filename] = times

outfile = "scaling.csv"
output = open(outfile, "w")
header = "algorithm,dataset,threads,time\n"
output.write(header);
offset_start = 0;
for algorithm, times in algotimes.iteritems():
  offset = 0
  for dataset in datasets:
    time = numpy.amin(times[offset:offset+5])
    offset += 5
    output.write("%s,%s,1,%f\n" %(algorithm, dataset, time))
output.close()
data = pandas.read_csv(outfile)
print data
